from flask import redirect, url_for, render_template, flash, request
from flask_login import LoginManager, UserMixin, login_user, logout_user,\
    current_user, login_required
from container_manager import *
from settings import app, db
import os

# initialize login manager
lm = LoginManager(app)
lm.login_view = 'login'

# Create database to hold user data
class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column('username', db.String(20), unique=True, index=True)
    password = db.Column('password', db.String(250))
    b2drop_name = db.Column(db.String(64), nullable=True)
    b2drop_pswd = db.Column(db.String(64), nullable=True)
    b2drop_url = db.Column(db.String(64), nullable=True)

    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.b2drop_url = ""
        self.b2drop_pswd = ""
        self.b2drop_name= ""

    def check_password(self , password):
        return self.password == password

@lm.user_loader
def load_user(id):
    return User.query.get(int(id))

@app.route('/')
@login_required
def index():
    return render_template('index.html')


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')

    username = request.form['username']
    password = request.form['password']

    registered_user = User.query.filter_by(username=username).first()

    if registered_user is None:
        flash('Username is invalid', 'error')
        return redirect(url_for('login'))
    if not registered_user.check_password(password):
        flash('Password is invalid','error')
        return redirect(url_for('login'))

    login_user(registered_user)
    flash('Logged in successfully')
    return redirect(url_for('index'))

@app.route('/register' , methods=['GET','POST'])
@login_required
def register():
    if request.method == 'GET':
        return render_template('register.html')

    try:

       request_url = requests.get(request.form['url'], auth=(request.form['uname'], request.form['pswd']))

       if request_url.status_code is 200:
           user = current_user
           user.b2drop_url = request.form['url']
           user.b2drop_name = request.form['uname']
           user.b2drop_pswd = request.form['pswd']

           db.session.commit()
           login_user(user, True)

           flash('User b2drop details successfully registered')
           return redirect(url_for('index'))
       else:
           flash('invalid b2drop details')
           return render_template('register.html')

    except requests.exceptions.RequestException as e:  # This is the correct syntax
       flash('invalid b2drop details')
       return render_template('register.html')

@app.route('/jupyter' , methods=['GET'])
@login_required
def run_jupyter():
    user = current_user
    notebook_url = create_jupyter_container(user)
#    flash('User Jupyter container successfully created')
    return redirect('http://193.167.189.21:8080%s'%notebook_url)

@app.route('/webodv' , methods=['GET'])
@login_required
def run_terminal():
    user = current_user
    terminal_url = create_webodv_container(user)
    print (terminal_url)
 #   flash('User Terminal container successfully created')
    return redirect('http://193.167.189.21:8080%s'%terminal_url)

## reads usernames and passwords from a file and populates database
def create_test_users(filename):
    pwd = os.path.dirname(__file__)
    with open(os.path.join(pwd, filename)) as f:
        for line in f:
            if not line:
                continue
            parts = line.split()
            name = parts[0]
            password = parts[1]
            user = User(name, password)
            db.session.add(user)
            db.session.commit()

if __name__ == '__main__':
    db.drop_all()
    db.create_all()
    create_test_users('userdata')
    app.run(host='0.0.0.0',debug=True)
