import docker
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# initialize flask app
app = Flask(__name__)
app.config['SECRET_KEY'] = 'sdcsecret!'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'

# database
db = SQLAlchemy(app)

# docker socket
client = docker.DockerClient(base_url='unix://docker.sock')

# proxy settings
proxy_url_endpoint = 'http://localhost:8001'
proxy_token = "716238957362948752139417234"
