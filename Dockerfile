FROM python:3.6

RUN apt-get update && apt-get install -y \
    vim \
    nodejs \
    npm

RUN npm install -g configurable-http-proxy

COPY ./ /opt/code

WORKDIR /opt/code

RUN pip install -r requirements.txt

EXPOSE 5000

CMD ["/bin/bash"]
