
import time
import docker
import json
import requests

from settings import client, proxy_url_endpoint


def create_jupyter_container(user):
    """
    If jupyter container is already running, just executes the b2drop script.
    Otherwise, create the container for user
    :param user:
    :return:
    """
    container_name = "jupyter_%s"%(user.username)
    notebook_serve_url = "/jupyter/{}".format(user.username)
    container_port = 8888

    if client.containers.list(filters={'name':container_name}):
        notebook = client.containers.list(filters={'name':container_name})[0]
        notebook.exec_run(
            cmd="/etc/init.d/start.sh {} {} {}".format(user.b2drop_name, user.b2drop_pswd, user.b2drop_url))        

        if (notebook.status != "running"):
            notebook.restart()
    else:

        try:
            # launch a container
            notebook = client.containers.run('vathsava/diva-test', detach=True, auto_remove=True,
                                              name=container_name, network="jupyterhub-network",
                                              cap_add=["SYS_ADMIN"], devices=["/dev/fuse"],
                                              command="start-notebook.sh --NotebookApp.base_url=%s --NotebookApp.token=''"%(
                                               notebook_serve_url))

            # execute the b2drop script
            notebook.exec_run(
            cmd="/etc/init.d/start.sh {} {} {}".format(user.b2drop_name, user.b2drop_pswd, user.b2drop_url))

        except docker.errors.APIError:
            print("Name error")

        #TODO: not ideal and need to be updated
        # wait until container starts
        time.sleep(5)


   ## proxy user container
    add_proxy_route(container_name, notebook_serve_url, container_port)

    return notebook_serve_url


def add_proxy_route(container_name, notebook_serve_url, port):

    #http_client = AsyncHTTPClient()
    #headers = {"Authorization": "token {}".format(proxy_token)}

    # create proxy endpoint
    proxy_endpoint = "{}/api/routes{}".format(proxy_url_endpoint, notebook_serve_url)
    body = json.dumps({
        "target": "http://{}:{}".format(container_name, port)
    })

    #post a request to proxy
    requests.post(proxy_endpoint, data=body)


def create_webodv_container(user):
    """
       If webodv container is already running, just executes the b2drop script.
       Otherwise, create the container for user
       :param user:
       :return:
       """
    vre_user = "VRE_USER=%s"%(user.username)
    container_name = "webodv_%s"%(user.username)
    webodv_serve_url = "/{}/webodv_extract_region".format(user.username)
    container_port = 80


    if client.containers.list(filters={'name':container_name}):
        notebook = client.containers.list(filters={'name':container_name})[0]
        notebook.exec_run(
            cmd="/etc/init.d/start.sh {} {} {}".format(user.b2drop_name, user.b2drop_pswd, user.b2drop_url))

        if (notebook.status != "running"):
            notebook.restart()

    else:

        try:
            notebook = client.containers.run('vathsava/webodv_proto', detach=True, auto_remove=True, name=container_name,
                                             network="jupyterhub-network", cap_add=["SYS_ADMIN"], devices=["/dev/fuse"],
                                             environment=[vre_user])

            notebook.exec_run(
            cmd="/etc/init.d/start.sh {} {} {}".format(user.b2drop_name, user.b2drop_pswd, user.b2drop_url))

        except docker.errors.APIError:
            print("Name error")

        time.sleep(5)


    ## proxy user container
    add_proxy_route(container_name, webodv_serve_url, container_port)

    return webodv_serve_url

